{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        haskell-env = pkgs.haskellPackages.ghcWithHoogle (hp:
          with hp; [
            ansi-terminal
            servant
            servant-server
            wai
            warp
            relude
            gloss
            
            # sdl2
            # sdl2-ttf
            # hspec
            # brick
            # cursor
            # microlens
            # microlens-platform
            # cursor
            # cursor-brick
            # path
            # path-io
            # pretty-show
            # text
            # skylighting
            # # brick-panes
            # brick-skylighting
            # linear
          ]);
        miscPkgs = with pkgs; [
          cabal-install
          hello
          # gcc
          ghcid
          # haskell-language-server
          hlint
          # ormolu
          # stylish-haskell
        ];
      in {
        # packages.aarch64-linux.tui = pkgs.stdenv.mkDerivation {
        #     name = "rgv";
        #     src = self;
        #     buildPhase = "ghc -threaded ./twoZeroFourEight/main.hs";
        #   };
        #   # nixpkgs.legacyPackages.aarch64-linux.hello;
        devShell = pkgs.mkShell { buildInputs = [ haskell-env ] ++ miscPkgs; };
        # defaultPackage.aarch64-linux = self.packages.aarch64-linux.hello;
      });
}
