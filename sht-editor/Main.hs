module Main where

import System.IO
  (BufferMode(NoBuffering),
   hFlush,
   stdout,
   hSetBuffering,
   stdin,
   hSetEcho,
   hIsReadable,
   hGetContents,
   openFile,
   hReady,
   IOMode(ReadWriteMode) )
import System.Environment (getArgs)
import qualified System.Console.ANSI as Terminal
import Data.Maybe (fromMaybe)
import System.Exit (exitFailure, exitSuccess)
import Data.Char (isControl, isSpace)

slice :: Int -> Int -> [a] -> [a]
slice startingFrom width = take width . drop startingFrom

preCurrAfter :: Int -> [a] -> ([a],a,[a])
preCurrAfter idx arr = (take idx arr, arr !! idx, drop (idx + 1) arr)

tabSize :: Int
tabSize = 4

topBarHeight :: Int
topBarHeight = 1

data CursorPos = CursorPos Int Int

data Model = Model
  { title :: String
  , terminalSize :: TerminalSize
  , currentFileName :: Maybe String
  , textBuffer :: [Sting]
  , cursorPos :: CursorPos
  }

data Msg =
  AddStr Int Int String
  | TypeChar Char
  | MoveCursorRelative CursorPos
  | Enter
  | Backspace
  | CutLine
  | Indent
  | Dedent
  | Resize TerminalSize


update :: Msg -> Model -> Model
update (TypeChar c) model =
  update (AddStr curR curC [c]) model {cursorPos = nextPos}
  where
    CursorPos curR curC = cursorPos model
    nextPos = CursorPos curR (curC + 1)

update (AddStr r c txt) model =
  model {textBuffer = updatedTextBuffer}
  where
    (pre,after) = splitAt r (textBuffer model)
    updatedTextBuffer = case after of
      [] -> pre <> [txt]
      (x:xs) -> pre <> [take c x <> txt <> drop c x] <> xs

update Enter model =
  model {cursorPos = updateCursorPos, textBuffer = updateTextBuffer}
  where
    CursorPos curR curC = cursorPos model
    tbuf = textBuffer model
