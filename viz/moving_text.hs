import Graphics.Gloss

data World = World
  { textX     :: Float
  , direction :: Float  -- +1 for right, -1 for left
  }

main :: IO ()
main = simulate
  (InWindow "Moving Text" 800 400 (10, 10))
  white
  60
  initialWorld
  draw
  update

initialWorld :: World
initialWorld = World
  { textX = -200
  , direction = 1
  }

draw :: World -> Picture
draw world = translate (textX world) 0 $ color blue $ scale 0.3 0.3 $ text "Moving Text"

update :: ViewPort -> Float -> World -> World
update _ deltaTime world = world { textX = newX, direction = newDirection }
  where
    -- Calculate the new x-coordinate
    speed = 100 -- Adjust this value for the desired speed
    newX = textX world + direction world * speed * deltaTime

    -- Change the direction when reaching the screen edges
    newDirection
      | newX >= 200 = -1
      | newX <= -200 = 1
      | otherwise = direction world
