import Control.Monad (unless)
import qualified SDL
import qualified SDL.TTF as TTF

myList :: [String]
myList = ["Item 2", "Item 2", "3", "4","5"]

main :: IO()
main = do
  SDL.initialize [SDL.InitVideo]
  TTF.initialize

  -- Create Window
  window <- SDL.createWindow "List Visualiztion" SDL.defaultWindow
  renderer <- SDL.createRenderer window  (-1) SDL.defaultRenderer

  font <- TTF.openFont "/home/vamshi/Downloads/iosevka.ttc" 24

  SDL.rendererDrawColor renderer SDL.$= SDL.V4 255 255 255 255
  SDL.clear renderer

  let drawList [] _ = return ()
      drawList (item:items) y = do
        surface <- TTF.renderTextSolid font item $ SDL.V4 0 0 0 255
        texture <- SDL.createTextureFromSurface renderer surface
          SDL.compy renderer texture Nothing (Just (SDL.Rectangle(SDL.P (SDL.V2 10 y)) (SDL.V2 300 40)))
          drawList items (y + 50)

  
  drawList myList 10

  SDL.present renderer

  let loop = do
        events <- SDL.pollEvents
        let quit = any (== SDL.QuitEvent) (map SDL.eventPayload events)
        unless quit loop

  loop

  SDL.destroyRenderer renderer
  SDL.destroyWindow window
  TTF.closeFont font
  TTF.quit
  SDL.quit
          

