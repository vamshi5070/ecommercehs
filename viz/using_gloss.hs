import Graphics.Gloss

main :: IO ()
main = display (InWindow "Drawing Text" (800, 600) (10, 10)) white drawing

drawing :: Picture
  drawing = pictures
  [ translate (-200) 0 $ color red $ scale 0.5 0.5 $ text "Hello, Gloss!"
  , translate (-100) (-100) $ color blue $ scale 0.3 0.3 $ text "This is a text example."
  ]
