
data Object a  = Object String a
  deriving (Eq,Show)

main = do
  putStrLn "Give a list"
  inputList <- getList
  putStrLn "Give a name"
  listName <- getLine
  let myObject = Object listName inputList
  putStrLn $ printEveryThing myObject
  putStrLn $ formatList inputList
  putStrLn "my emacs"

formatList :: [Int] -> String
formatList xs = "[" <> listOfStrings <> "]"
  where listOfStrings = foldr (\x acc ->  (show x) <> ", " <> acc) "" xs

getList :: IO [Int]
getList = do
  rawInput <- getLine
  return ((map read $ words rawInput))

printEveryThing :: Object [Int] -> String
printEveryThing (Object name obj) = name <> ": " <> formatList obj
  
