{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE NoImplicitPrelude #-}

import Servant
import Network.Wai.Handler.Warp
import Network.Wai
import Relude

type HelloWorldAPI = "hello" :> Get '[JSON] Text

helloWorldAPI :: Proxy HelloWorldAPI
helloWorldAPI = Proxy

server :: Server HelloWorldAPI
server = return "Hello, World!"

app :: Application
app = serve helloWorldAPI server

main :: IO ()
main = run 8080 app
